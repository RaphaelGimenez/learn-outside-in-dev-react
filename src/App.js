import React from 'react';
import RestaurantScreen from './components/RestaurantScreen';
import {Provider} from 'react-redux';
import store from './store';

const App = () => (
  <div>
    <Provider store={store}>
      <RestaurantScreen />
    </Provider>
  </div>
);

export default App;
